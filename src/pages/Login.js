import React from 'react';
import { PostData } from '../services/PostData';
import { Redirect } from 'react-router-dom';


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		"username": '',
		"useremail": '',
		"password": '',
		"redirect": false,
		"message": '',
		"error" : ''
	}
	this.login = this.login.bind(this);
	this.handleSignUpSubmit = this.handleSignUpSubmit.bind(this);
	this.onChange = this.onChange.bind(this);
  }

  login(e){
	e.preventDefault();
	if(this.state.useremail && this.state.password){
		let data = JSON.stringify(this.state);
		PostData('login.php', data, 'POST').then ((result) =>{
		let responseJson = result;
		if(responseJson.userdata){
			sessionStorage.setItem('userdata', responseJson.userdata)
			sessionStorage.setItem('userId', responseJson.userdata.id)
			sessionStorage.setItem('userName', responseJson.userdata.uname)
			sessionStorage.setItem('_token', responseJson.userdata._token)
			this.setState({redirect: true})
		}else{
			console.log("Login Error!")
		}
		
		});
	}
  }

  onChange(e){
	  this.setState({[e.target.name] : e.target.value});
  }

  handleSignUpSubmit(e){
	  e.preventDefault();
	  let data = JSON.stringify(this.state);
		PostData('signup.php', data, 'POST').then ((result) =>{
		let responseJson = result;
		if(responseJson.userdata){
			this.setState({message:responseJson.userdata})
		}else{
			this.setState({message:responseJson.error})
		}
	});
  }

  render() {

	if(this.state.redirect){
		return (<Redirect to={'/'}/>)
	}
    return (
		<section id="form">
		<div className="container">
			<div className="row">
				<div className="col-sm-4 col-sm-offset-1">
					<div className="login-form">
						<h2>Login to your account</h2>
						<form>
						<input type="email" name="useremail" placeholder="Email Address" onChange={this.onChange}/>
						<input type="password" name="password" placeholder="Password" onChange={this.onChange}/>
						<span>
							<input type="checkbox" className="checkbox"/> 
							Keep me signed in
						</span>
						<button type="submit" className="btn btn-default" onClick={this.login}>Login</button>
						</form>
					</div>
				</div>
				<div className="col-sm-1">
					<h2 className="or">OR</h2>
				</div>
				<div className="col-sm-4">
					<div className="signup-form">
						<h2>New User Signup!</h2>
						{ this.state.message &&
							<h4 className="text-success text-center">{this.state.message}</h4>
						}

						{ this.state.error &&
							<h4 className="text-danger text-center">{this.state.error}</h4>
						}
						
						<form action="#" onSubmit={this.handleSignUpSubmit}>
							<input type="text" name="username" placeholder="Name" onChange={this.onChange}/>
							<input type="email" name="useremail" placeholder="Email Address" onChange={this.onChange}/>
							<input type="password" name="password" placeholder="Password" onChange={this.onChange}/>
							<button type="submit" className="btn btn-default">Signup</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
    );
  }
}

export default Login;
