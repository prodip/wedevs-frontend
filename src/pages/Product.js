import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { BASE_API } from '../services/Constants';

class Product extends Component
{
    state = {
        inCart: this.props.inCart
    };

    addToCart = (e) => {
        e.preventDefault();
        this.props.addToCart(this.props.product)
        this.setState({
            inCart: true
        })
    }

    render() {
        const { product } = this.props;
        return (
            <>
            <div className="col-sm-4">
            <Link to={{pathname: `product-detail/${product.id}`}}>	
                <div className="product-image-wrapper">
                    <div className="single-products">
                            <div className="productinfo text-center">
                                <div className="">
                                <img src={BASE_API + product.image} alt={product.name} height="240px" />
                                </div>
                                <h2>${product.price}</h2>
                                <p>{product.name}</p>
                                {
                                    this.state.inCart?(
                                        <span className="btn btn-success add-to-cart">Added to cart</span>
                                    ) : (
                                        <a href="#" onClick={this.addToCart} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart"></i>Add to cart</a>
                                    )
                                }                            
                            </div>
                            <div className="product-overlay">
                                <div className="overlay-content">
                                <h2>${product.price}</h2>
                                <p>{product.name}</p>
                                {
                                    this.state.inCart?(
                                        <span className="btn btn-success add-to-cart">Added to cart</span>
                                    ) : (
                                        <a href="#" onClick={this.addToCart} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart"></i>Add to cart</a>
                                    )
                                }
                                </div>
                            </div>
                    </div>
                    <div className="choose">
                        <ul className="nav nav-pills nav-justified">
                            <li><a href="#"><i className="fa fa-plus-square"></i>Add to wishlist</a></li>
                            <li><a href="#"><i className="fa fa-plus-square"></i>Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </Link>
            </div>
        </>
        )
    }
}

export default Product;