import React from 'react';
import { connect } from 'react-redux';
import { PostData } from '../services/PostData';
import { addToCart } from '../services/CartActions';
import Sidebar from '../inc/Sidebar';
import Product from './Product';

class Shop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		products:'',
	}
  }

  componentDidMount(){
	PostData('product.php', '', 'GET').then ((result) =>{
		let responseJson = result;
		console.log(responseJson);
		console.log(responseJson.products);
		this.setState({products:responseJson.products})
	});
	}

	addToCart = (product) => {
		this.props.addToCart(product);
	}


render() {

	const {products} = this.state;

    return (
	<div>
		<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
				<Sidebar/>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items">
						<h2 class="title text-center">Features Items</h2>

						{products.length &&
						<>
							{
                        		products.map(product => <Product product={product} addToCart={this.addToCart} inCart={this.props.cart.length>0 && this.props.cart.filter(e => e.product.id === product.id).length > 0 } key={product.id} /> )
                    		}
						</>
						}
					</div>
				</div>
			</div>
		</div>
	  </section>
	</div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        products: state.product.products,
        cart: state.cart.cart
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product) => {
            dispatch(addToCart(product));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop)
