import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateCartQuantity, removeFromCart } from '../services/CartActions';
import { BASE_API } from '../services/Constants';
import { Link } from 'react-router-dom';


class Item extends Component {

    constructor(props) {
        super(props);

        this.state = {
            quantity: this.props.item.quantity,
            tPrice: this.props.item.tPrice,
            btnVisible: false
        };
    }

    handleChange = (e) => {

        if(e.target.value <= 0) {
            alert("Quantity must be greater than or equal to 1");
            return;
        }
        if(e.target.value > this.props.item.product.amount) {
            alert("You have exceeded the available items of this product!");
            return;
        }
        if(this.state.quantity != e.target.value) {
            let totalPrice = this.props.item.product.price * e.target.value;
            this.setState({
                quantity: e.target.value,
                tPrice: totalPrice,
        });
            this.handleSubmit(e.target.value, totalPrice);
        }
    }

    handleSubmit = (qty, tPrice) => {
        this.props.updateCartQuantity(this.props.item.product.id, qty, tPrice);
    }

    handleRemove = (e) => {
        e.preventDefault();
        this.props.removeFromCart(this.props.item.product.id);
    }

  render() {

      const { item } = this.props;

      return (
            <>
            <tr>
                <td className="cart_product">
                    <Link to={{pathname: `product-detail/${item.product.id}`}}>
                        <img src={BASE_API + item.product.image} alt={item.product.name} height="200"/>
                    </Link>
                </td>
                <td className="cart_description">
                    <h4><a href="">{item.product.name}</a></h4>
                    <p>SKU ID: {item.product.sku}</p>
                </td>
                <td className="cart_price">
                    <p>${item.product.price}</p>
                </td>
                <td className="cart_quantity">
                <form onSubmit={this.handleSubmit}>
                    <div className="cart_quantity_button d-flex">
                        <input className="cart_quantity_input w-100" type="number" name="quantity" value={this.state.quantity}  onChange={this.handleChange} autoComplete="off" size="2"/>
                    </div>
                    {
                          this.state.btnVisible?(
                              <div className="col-xs-2">
                                  <button type="submit" className="btn btn-primary">Update</button>
                              </div>
                          ) : null
                    }
                </form>
                </td>
                <td className="cart_total">
                    <p className="cart_total_price">${this.state.tPrice}</p>
                </td>
                <td className="cart_delete">
                    <a className="cart_quantity_delete" href="#" onClick={this.handleRemove}><i className="fa fa-times"></i></a>
                </td>
            </tr>
          </>
      )
  }
}

const mapDispatchToProps = (dispatch) => {

    return {
        updateCartQuantity: (productId, quantity, tPrice) => dispatch(updateCartQuantity(productId, quantity, tPrice)),
        removeFromCart: (productId) => dispatch(removeFromCart(productId))
    }
};

export default connect(null, mapDispatchToProps)(Item);