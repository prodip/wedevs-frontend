import React from 'react';
import Item from './Item';
import { PostData } from '../services/PostData';
import { connect } from 'react-redux';

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		loginReq: false,
		_token: sessionStorage.getItem("_token"),
		orderCompleted: false
		
	}
	this.checkout = this.checkout.bind(this)
  }

  checkout(e){
	  e.preventDefault();
	  let uToken = sessionStorage.getItem("_token");
	  if(!uToken){
		this.setState({loginReq: true});
		return false;
	  }

	  let cartOrder = {
		  _token: this.state._token,
		  cart: this.props.cart
	  }

	  console.log('clicked checkout')
	  console.log('cart user', this.state.user)
	  console.log('cart view', this.props.cart)
	  console.log('cart order', cartOrder)
	  
	  cartOrder = JSON.stringify(cartOrder);
	  PostData('order.php', cartOrder, 'POST').then ((result) =>{
		let responseJson = result;
		console.log(responseJson);
		console.log(responseJson.products);
		this.setState({products:responseJson.products})
		this.setState({orderCompleted: true})
	  });
	 
  }

  render() {
	let total = 0;
	this.props.cart.map(item => total += item.product.price * item.quantity);
	const cart  = this.props.cart.length > 0?(
		<div>
		<div className="table-responsive cart_info">
				<table className="table table-condensed">
					<thead>
						<tr className="cart_menu">
							<td className="image">Item</td>
							<td className="description"></td>
							<td className="price">Price</td>
							<td className="quantity">Quantity</td>
							<td className="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					{
						this.props.cart.map(item => {
							return (
								<Item item={item} key={item.product.id}/>
							)
						})
					}
					</tbody>
				</table>
			</div>
		</div>

	) : (
		<div className="panel-body">
			<p>Cart is empty</p>
		</div>
	)

    return (
		<div>
		<section id="cart_items">
		<div className="container">
			<div className="breadcrumbs">
				<ol className="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li className="active">Shopping Cart</li>
				</ol>
			</div>

			{ cart }

		</div>
	</section> 

	<section id="do_action">
		<div className="container">
			<div className="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div className="row">
				<div className="col-sm-6">
					<div className="chose_area">
						<ul className="user_option">
							<li>
								<input type="checkbox"/>
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox"/>
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox"/>
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul className="user_info">
							<li className="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li className="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li className="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text"/>
							</li>
						</ul>
					</div>
				</div>
				<div className="col-sm-6">
					<div className="total_area">
						{ this.state.loginReq &&
								<h4 className="err-text">Please Login for checkout!</h4>
						}

						{ this.state.orderCompleted &&
								<h4 className="text-success text-center">Your Order Successfully sent</h4>
						}
						
						<ul>
							<li>Cart Sub Total <span>${total.toFixed(2)}</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>${(total+2).toFixed(2)}</span></li>
						</ul>
						{
							this.state.orderCompleted?(
								<div className="btn btn-success ml-4">Completed</div>
							) : (
								<a className="btn btn-default update" href="#" onClick={this.checkout}>Check Out</a>
							)
						}
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
    );
  }
}

const mapStateToProps = (state) => {
	return {
		cart: state.cart.cart
	}
  };
  
  export default connect(mapStateToProps)(Cart);
