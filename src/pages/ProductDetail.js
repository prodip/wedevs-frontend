import React from 'react';
import { connect } from 'react-redux';
import { updateCartQuantity, removeFromCart } from '../services/CartActions';
import { PostData } from '../services/PostData';
import { BASE_API } from '../services/Constants';
import { addToCart } from '../services/CartActions';
import Sidebar from '../inc/Sidebar';


class ProductDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
		products:'',
		product:'',
		quantity: 1,
		tPrice: '',
		inCart: false
	}
	this.onChange = this.onChange.bind(this);
  }

  async componentDidMount(){
	const id = this.props.match.params.id;
	let url = `product-details.php?id=${id}`;
	PostData(url, '', 'GET').then ((result) =>{
		let responseJson = result;
		this.setState({product:responseJson.product})
		this.setState({tPrice:responseJson.product.price})
		this.productInCart();
	});	
	}

	productInCart(){
	if(this.props.cart.length>0 && this.props.cart.filter(e => e.product.id == this.state.product.id).length > 0 ){
		this.setState({inCart: true})
	}
	}

	handleChange = (e) => {

        if(e.target.value <= 0) {
            alert("Quantity must be greater than or equal to 1");
            return;
        }
        if(e.target.value > this.state.product.amount) {
            alert("You have exceeded the available items of this product!");
            return;
        }
        if(this.state.quantity != e.target.value) {
            let totalPrice = this.state.product.price * e.target.value;
            this.setState({
                quantity: e.target.value,
                tPrice: totalPrice,
            });
        }

    }

    handleSubmit = (e) => {
        e.preventDefault();
        let tPrice = this.state.product.price * this.state.quantity;
        this.props.updateCartQuantity(this.state.product.id, this.state.quantity, tPrice);
    }

	onChange(e){
		e.preventDefault();
		let quantity = e.target.value;
		let price = this.state.product.price;
		let tPrice = price * quantity;
		this.setState({quantity :quantity});
		this.setState({tPrice : tPrice});
        updateCartQuantity(this.state.product.id, this.state.quantity, tPrice);
	}

	addToCart = (e) => {
		this.props.addToCart(this.state.product);
		this.handleSubmit(e);
		this.productInCart();
	}


  render() {

	const {product, quantity, tPrice, inCart} = this.state 
    return (
	<section>
		<div className="container">
			<div className="row">
				<div className="col-sm-3">
				<Sidebar/>
				</div>
				
				<div className="col-sm-9 padding-right">
				{/* <!--product-details--> */}
					<div className="product-details">
						<div className="col-sm-5">
							<div className="view-product">
								<div className="">
								<img src={BASE_API + product.image} alt={product.name} />
								</div>
							</div>
							<div id="similar-product" className="carousel slide" data-ride="carousel">
								
								  {/* <!-- Wrapper for slides --> */}
								    <div className="carousel-inner">
										<div className="item active">
										  <a href=""><img src="/images/product-details/similar1.jpg" alt=""/></a>
										  <a href=""><img src="/images/product-details/similar2.jpg" alt=""/></a>
										  <a href=""><img src="/images/product-details/similar3.jpg" alt=""/></a>
										</div>
										<div className="item">
										  <a href=""><img src="/images/product-details/similar1.jpg" alt=""/></a>
										  <a href=""><img src="/images/product-details/similar2.jpg" alt=""/></a>
										  <a href=""><img src="/images/product-details/similar3.jpg" alt=""/></a>
										</div>
										<div className="item">
										  <a href=""><img src="/images/product-details/similar1.jpg" alt=""/></a>
										  <a href=""><img src="/images/product-details/similar2.jpg" alt=""/></a>
										  <a href=""><img src="/images/product-details/similar3.jpg" alt=""/></a>
										</div>
									</div>
								  {/* <!-- Controls --> */}
								  <a className="left item-control" href="#similar-product" data-slide="prev">
									<i className="fa fa-angle-left"></i>
								  </a>
								  <a className="right item-control" href="#similar-product" data-slide="next">
									<i className="fa fa-angle-right"></i>
								  </a>
							</div>
						</div>
						<div className="col-sm-7">
						{/* <!--/product-information--> */}
							<div className="product-information">
								<img src="/images/product-details/new.jpg" className="newarrival" alt="" />
								<h2>{product.name}</h2>
								<p>SKU ID: {product.sku}</p>
								<div>
								<img src="/images/product-details/rating.png" alt="" />
								</div>
								<span>
									<span>US ${tPrice}</span>
								</span>
								<span style={{marginTop: 0}}>
									<label>Quantity:</label>
									<input type="number" name="quantity" value={this.state.quantity} onChange={this.handleChange}/>
									{
                                    this.state.inCart?(
                                        <div className="btn btn-success cart disabled">Added to cart</div>
                                    ) : (
                                        <div onClick={this.addToCart} className="btn btn-default cart"><i className="fa fa-shopping-cart"></i>Add to cart</div>
                                    )
                                	}

								</span>
								<p><b>Availability:</b> In Stock</p>
								<p><b>Condition:</b> New</p>
								<p><b>Brand:</b> {product.category_name}</p>
								<a href=""><img src="/images/product-details/share.png" className="share img-responsive"  alt="" /></a>
							</div>
							{/* <!--/product-information--> */}
						</div>
					</div>
					{/* <!--/product-details--> */}
					
					{/* <!--category-tab--> */}
					<div className="category-tab shop-details-tab">
						<div className="col-sm-12">
							<ul className="nav nav-tabs">
								<li><a href="#details" data-toggle="tab">Details</a></li>
								<li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
								<li><a href="#tag" data-toggle="tab">Tag</a></li>
								<li className="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
							</ul>
						</div>
						<div className="tab-content">
							<div className="tab-pane fade" id="details" >
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div className="tab-pane fade" id="companyprofile" >
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div className="tab-pane fade" id="tag" >
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-3">
									<div className="product-image-wrapper">
										<div className="single-products">
											<div className="productinfo text-center">
												<img src="/images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div className="tab-pane fade active in" id="reviews" >
								<div className="col-sm-12">
									<ul>
										<li><a href=""><i className="fa fa-user"></i>EUGEN</a></li>
										<li><a href=""><i className="fa fa-clock-o"></i>12:41 PM</a></li>
										<li><a href=""><i className="fa fa-calendar-o"></i>31 DEC 2014</a></li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
									<p><b>Write Your Review</b></p>
									
									<form action="#">
										<span>
											<input type="text" placeholder="Your Name"/>
											<input type="email" placeholder="Email Address"/>
										</span>
										<textarea name="" ></textarea>
										<b>Rating: </b> <img src="/images/product-details/rating.png" alt="" />
										<button type="button" className="btn btn-default pull-right">
											Submit
										</button>
									</form>
								</div>
							</div>
							
						</div>
					</div>
					{/* <!--/category-tab--> */}
					
					{/* <!--recommended_items--> */}
					<div className="recommended_items">
						<h2 className="title text-center">recommended items</h2>
						
						<div id="recommended-item-carousel" className="carousel slide" data-ride="carousel">
							<div className="carousel-inner">
								<div className="item active">	
									<div className="col-sm-4">
										<div className="product-image-wrapper">
											<div className="single-products">
												<div className="productinfo text-center">
													<img src="/images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													
												</div>
											</div>
										</div>
									</div>
									<div className="col-sm-4">
										<div className="product-image-wrapper">
											<div className="single-products">
												<div className="productinfo text-center">
													<img src="/images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													
												</div>
											</div>
										</div>
									</div>
									<div className="col-sm-4">
										<div className="product-image-wrapper">
											<div className="single-products">
												<div className="productinfo text-center">
													<img src="/images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="item">	
									<div className="col-sm-4">
										<div className="product-image-wrapper">
											<div className="single-products">
												<div className="productinfo text-center">
													<img src="/images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													
												</div>
											</div>
										</div>
									</div>
									<div className="col-sm-4">
										<div className="product-image-wrapper">
											<div className="single-products">
												<div className="productinfo text-center">
													<img src="/images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													
												</div>
											</div>
										</div>
									</div>
									<div className="col-sm-4">
										<div className="product-image-wrapper">
											<div className="single-products">
												<div className="productinfo text-center">
													<img src="/images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							 <a className="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i className="fa fa-angle-left"></i>
							  </a>
							  <a className="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i className="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div>
					{/* <!--/recommended_items--> */}
					
				</div>
			</div>
		</div>
	</section>
	
    );
  }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart.cart
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product) => {
            dispatch(addToCart(product));
        },
        updateCartQuantity: (productId, quantity, tPrice) => dispatch(updateCartQuantity(productId, quantity, tPrice)),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails)

