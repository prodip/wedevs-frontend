import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import Header from './inc/Header';
import Slider from './inc/Slider';
import Footer from './inc/Footer';
import Home from './pages/Home';
import Cart from './pages/Cart';
import ProductDetail from './pages/ProductDetail';
import Checkout from './pages/Checkout';
import Login from './pages/Login';
import Product from './pages/Shop';

//import logo from './logo.svg';
import './App.css';

function App() {
  return (

    <Router>

      <Header/>

     {/* <TransitionGroup>
        <CSSTransition
          timeout={300}
          classNames='fade'
        >
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route path='/cart' component={Cart}/>
        <Route path='/product' component={Product}/>
        <Route path='/product-detail' component={ProductDetail}/>
        <Route path='/checkout' component={Checkout}/>
        <Route path='/login' component={Login}/>
        {/* <RedirectLoggedIn exact path={ROUTE_LOGIN} component={Signin}/>
        <RedirectLoggedIn exact path='/signup' component={Signup}/>

        <PrivateRoute exact path={ROUTE_HOME} component={Home}/>
        <PrivateRoute exact path='/jobs' component={Jobs}/> */}
     {/* </Switch>
      </CSSTransition>
      </TransitionGroup> */}

      <Route
          render={({ location }) => (
            <TransitionGroup>
              {/* React router transitions */}
              <CSSTransition key={location.key} appear in timeout={300} classNames="fade">
                <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/cart' component={Cart}/>
                <Route path='/product' component={Product}/>
                <Route exact path="/product-detail/:id" component={ProductDetail}></Route>
                <Route path='/checkout' component={Checkout}/>
                <Route path='/login' component={Login}/>
                </Switch>
              </CSSTransition>
            </TransitionGroup>
          )}
        />

        <Footer/>

    </Router>

  );
}

export default App;
