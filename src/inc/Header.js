import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		userdata: '',
		userId: '',
		userName: '',
		_token: '',
	}
	this.logout = this.logout.bind(this)
  }

  componentDidMount(){
	  if(sessionStorage.getItem("userdata")){
		this.setState({userdata: sessionStorage.getItem("userdata")});
		this.setState({userId: sessionStorage.getItem("userId")});
		this.setState({userName: sessionStorage.getItem("userName")});
		this.setState({_token: sessionStorage.getItem("_token")});
	  }
  }

  logout(e){
	e.preventDefault();
	sessionStorage.setItem('userdata', '')
	sessionStorage.setItem('userId', '')
	sessionStorage.setItem('userName', '')
	sessionStorage.setItem('_token', '')
	sessionStorage.clear();
	this.setState({_token: ''})
  }

  render() {

    return (
        <header id="header">
		<div className="header_top">
			<div className="container">
				<div className="row">
					<div className="col-sm-6">
						<div className="contactinfo">
							<ul className="nav nav-pills">
								<li><a href="#"><i className="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i className="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="social-icons pull-right">
							<ul className="nav navbar-nav">
								<li><a href="#"><i className="fa fa-facebook"></i></a></li>
								<li><a href="#"><i className="fa fa-twitter"></i></a></li>
								<li><a href="#"><i className="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i className="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		{/* <!--/header_top--> */}
		
		{/* <!--header-middle--> */}
		<div className="header-middle">
			<div className="container">
				<div className="row">
					<div className="col-md-4 clearfix">
						<div className="logo pull-left">
							<Link to="/"><img src="images/home/logo.png" alt="" /></Link>
						</div>
					</div>
					<div className="col-md-8 clearfix">
						<div className="shop-menu clearfix pull-right">
							<ul className="nav navbar-nav">
								<li><Link to="/cart"><i className="fa fa-crosshairs"></i> Checkout</Link></li>
								<li>
								<Link to="/cart">
								{
                                    this.props.cart && this.props.cart.length > 0 ? (
                                        <span className="cart-badge">{ this.props.cart.length }</span>
                                    ) : null
                                }
								<i className="fa fa-shopping-cart"></i> Cart
								</Link>
								</li>
								{this.state._token ? (
								<li><a href="#" onClick={this.logout}><i className="fa fa-lock"></i> logout</a></li>
								) : (
								<li><Link to="/login"><i className="fa fa-lock"></i> Login</Link></li>
								)}
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		{/* <!--/header-middle--> */}
	
		{/* <!--header-bottom--> */}
		<div className="header-bottom">
			<div className="container">
				<div className="row">
					<div className="col-sm-9">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
						</div>
						<div className="mainmenu pull-left">
							<ul className="nav navbar-nav collapse navbar-collapse">
								<li><Link to="/" className="active">Home</Link></li>
								<li className="dropdown"><a href="#">Shop<i className="fa fa-angle-down"></i></a>
                                    <ul role="menu" className="sub-menu">
                                        <li><Link to="/product">Products</Link></li>
										<li><Link to="/checkout">Checkout</Link></li> 
										<li><Link to="/cart">Cart</Link></li> 
										<li><Link to="/login">Login</Link></li> 
                                    </ul>
                                </li> 
							</ul>
						</div>
					</div>
					<div className="col-sm-3">
						<div className="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart.cart,
        cartUpdated: () => { return true }
    }
};

export default connect(mapStateToProps)(Header);
