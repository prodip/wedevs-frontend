export const addToCart = (product) => {

    return {
        type: 'ADD_TO_CART',
        payload: {
            product,
            quantity: 1,
            tPrice: product.price
        }
    }
};

export const removeFromCart = (productId) => {

    return {
        type: 'REMOVE_FROM_CART',
        payload: {
            productId: productId
        }
    }
};

export const updateCartQuantity = (productId, quantity, tPrice) => {

  return {
      type: 'UPDATE_CART_QUANTITY',
      payload: {
          productId,
          quantity: parseInt(quantity),
          tPrice: parseInt(tPrice)
      }
  }
};