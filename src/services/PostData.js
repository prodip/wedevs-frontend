export function PostData(type, userData, method){

    let BaseUrl = 'http://localhost/weDevs-backend/views/api/';

    if(method == 'POST' || method == 'PUT'){
        return new Promise((resolve, reject) => {
            fetch(BaseUrl+type,
            {   
                method: method,
                headers: {
                "Content-Type": "application/json",
                "Accept": 'application/json'
            },
            body: userData
            })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                reject(error)
            });
    
        });

    }else{
        return new Promise((resolve, reject) => {
            fetch(BaseUrl+type,
            {   
                method: method,
                headers: {
                "Content-Type": "application/json",
                "Accept": 'application/json'
            },
            })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                reject(error)
            });

    });

    }

}