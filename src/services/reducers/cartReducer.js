const initialState = {
  cart: []
};

const cartReducer = (state = initialState, action) => {

    let cart = state.cart;

    switch(action.type) {

        case 'ADD_TO_CART':

            cart.push(action.payload);

            return {
                ...state,
                cart: cart
            };
        case 'UPDATE_CART_QUANTITY':

            let newCart = Object.assign([], cart.map(item => {
                if(item.product.id == action.payload.productId){
                    item.product = item.product;
                    item.quantity = action.payload.quantity;
                    item.tPrice = action.payload.tPrice;
                    return item;
                }else{
                    return item;
                }
            }));

            return {
                ...state,
                cart: newCart
            };

        case 'REMOVE_FROM_CART':
            return {
                ...state,
                cart: cart.filter(item => item.product.id != action.payload.productId)
            };
        default:
            return state;
    }
};

export default cartReducer;